#!/usr/bin/env sh
set -eu
set -x

CI_REGISTRY_IMAGE='registry.gitlab.com/treebeardtech/cdk-spike'
CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:='local'}
CI_DEFAULT_BRANCH=${CI_DEFAULT_BRANCH:='master'}
CI_COMMIT_SHA=${CI_COMMIT_SHA:=$(git rev-parse HEAD)}

CI=${CI:=false}

if [ "${CI}" = true ]; then
  docker pull "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" \
    || docker pull "${CI_REGISTRY_IMAGE}:${CI_DEFAULT_BRANCH}" \
    || true
fi

export DOCKER_BUILDKIT=1
docker build \
  --cache-from "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" \
  --cache-from "${CI_REGISTRY_IMAGE}:${CI_DEFAULT_BRANCH}" \
  --tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" \
  --tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" \
  --build-arg BUILDKIT_INLINE_CACHE=1 \
  .devcontainer

if [ "${CI}" = true ]; then
  docker push "${CI_REGISTRY_IMAGE}" --all-tags
fi