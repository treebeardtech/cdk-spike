from aws_cdk import (
    Stack,
    aws_sso as sso
)
from constructs import Construct

INSTANCE_ARN = 'arn:aws:sso:::instance/ssoins-753552eee9003ece'
ALEX = '9c671859f8-e76b10d6-3875-4239-ad6f-b59b7e1a47e8'
DEV_ACCOUNT = '467373337646'

class SpikeStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        admin_permission_set = sso.CfnPermissionSet(
            self,
            'admin',
            name='Administrator',
            description='admins perms',
            instance_arn=INSTANCE_ARN,
            managed_policies=[
                'arn:aws:iam::aws:policy/AdministratorAccess'
            ]
        )

        sso.CfnAssignment(
            self,
            "alex-dev-assignment",
            instance_arn=INSTANCE_ARN,
            principal_id=ALEX,
            principal_type="USER",
            target_type="AWS_ACCOUNT",
            target_id=DEV_ACCOUNT,
            permission_set_arn=admin_permission_set.attr_permission_set_arn
        )
