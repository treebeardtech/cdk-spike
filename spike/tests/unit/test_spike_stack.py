import aws_cdk as core
import aws_cdk.assertions as assertions

from spike.spike_stack import SpikeStack

# example tests. To run these tests, uncomment this file along with the example
# resource in spike/spike_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = SpikeStack(app, "spike")
    template = assertions.Template.from_stack(stack)

    template.resource_count_is("AWS::SSO::PermissionSet", 1)